import { commonsOutputDebug, commonsOutputSetDebugging } from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';

import { PixeliseApp } from './apps/pixelise.app';

import { DatabaseService } from './services/database.service';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);

const app: PixeliseApp = new PixeliseApp();

const databaseService: DatabaseService = new DatabaseService({
		name: 'thumbnailpixeliser'
});

app.setDatabaseService(databaseService);

(async (): Promise<void> => {
	commonsOutputDebug('Starting application: Thumbnail Pixeliser');
	await app.start();
	commonsOutputDebug('Application completed');
})();
