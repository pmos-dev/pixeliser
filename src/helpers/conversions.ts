import { IRgb } from 'tscommons-es-graphics';

export function rgbMatrixToLinearNumberArray(rgb: IRgb[][]): number[] {
	const values: number[] = [];
	for (const ys of rgb) {
		for (const c of ys) {
			values.push(
					c.red,
					c.green,
					c.blue
			);
		}
	}
	
	return values;
}

export function linearNumberArrayToRgbMatrix(
		values: number[],
		divisionsX: number,
		divisionsY: number
): IRgb[][] {
	const matrix: IRgb[][] = [];
	for (let x = divisionsX; x-- > 0;) {
		const row: IRgb[] = [];
		for (let y = divisionsY; y-- > 0;) {
			row.push({
					// tslint:disable:object-literal-sort-keys
					red: values.shift()!,
					green: values.shift()!,
					blue: values.shift()!
					// tslint:enable:object-literal-sort-keys
			});
		}
		matrix.push(row);
	}
	
	return matrix;
}
