import { commonsAsyncInterval } from 'tscommons-es-async';
import { commonsAsyncAbortInterval } from 'tscommons-es-async';
import { ECommonsInvocation } from 'tscommons-es-async';
import { IRgb } from 'tscommons-es-graphics';

import { CommonsBareApp, assertSet } from 'nodecommons-es-app';
import {
		commonsOutputDoing,
		commonsOutputPercent,
		commonsOutputSuccess,
		commonsOutputDebug
} from 'nodecommons-es-cli';

import { Jimp } from '../classes/jimp';
import { Thumbnails } from '../classes/thumbnails';

import { DatabaseService } from '../services/database.service';

export class PixeliseApp extends CommonsBareApp {
	private databaseService: DatabaseService|undefined;
	private thumbnails: Thumbnails|undefined;
	
	constructor() {
		super();
	}
	
	public setDatabaseService(databaseService: DatabaseService): void {
		this.databaseService = databaseService;
	}
	
	public async apply(
			image: Jimp,
			blocksX: number,
			blocksY: number,
			divisionsX: number,
			divisionsY: number,
			destWidth: number,
			destHeight: number
	): Promise<Jimp> {
		if (!this.thumbnails) throw new Error('No thumbnails set. This should not be possible');
		
		const blockW: number = destWidth / blocksX;
		const blockH: number = destHeight / blocksY;

		const pixels: IRgb[][][][] = await image.pixelise(
				blocksX,
				blocksY,
				divisionsX,
				divisionsY
		);
		
		commonsOutputDoing(`Building applicants`);
		type TApplicant = {
				x: number;
				y: number;
				w: number;
				h: number;
				pixel: IRgb[][];
		};
		const applicants: TApplicant[] = [];
		{
			let x: number = 0;
			for (const sub of pixels) {
				let y: number = 0;
				for (const pixel of sub) {
					const tx: number = Math.floor(x);
					const ty: number = Math.floor(y);
					let tw: number = Math.floor(blockW) + 1;	// this +1 is to avoid floating point round missing bits
					let th: number = Math.floor(blockH) + 1;	// this +1 is to avoid floating point round missing bits
					
					while ((tx + tw) > destWidth) tw--;
					if (tw === 0) continue;
					
					while ((ty + th) > destHeight) th--;
					if (th === 0) continue;
					
					applicants.push({
							x: tx,
							y: ty,
							w: tw,
							h: th,
							pixel: pixel
					});
					
					y += blockH;
				}
				x += blockW;
			}
		}
		commonsOutputSuccess();
		
		const applied: Jimp = new Jimp();
		applied.createNew(destWidth, destHeight);
		
		commonsOutputDoing(`Applying thumbnails to image`);

		const total: number = applicants.length;
		let done: number = 0;
		
		commonsAsyncInterval(
				1000,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {
					commonsOutputPercent(done, total);
				},
				'apply-progress'
		);
		
		while (applicants.length > 0) {
			const index: number = Math.floor(Math.random() * applicants.length);
			const applicant: TApplicant = applicants.splice(index, 1)[0];

			const closest: string|undefined = this.thumbnails.getClosest(applicant.pixel);
			if (!closest) throw new Error('No closest thumbnail. Maybe pool is not big enough?');
			
			const thumbnail: Jimp = await this.thumbnails.loadThumbnail(closest);
			await thumbnail.resize(applicant.w, applicant.h);
			
			await applied.blit(
					thumbnail,
					applicant.x,
					applicant.y
			);

			done++;
		}
		
		commonsAsyncAbortInterval('apply-progress');
		
		commonsOutputSuccess();
		
		return applied;
	}
	
	protected async run(): Promise<void> {
		assertSet(this.databaseService, 'DatabaseService');

		const thumbnailsPath: string = this.getArgs().getString('thumbnails');
		const divisionsX: number = this.getArgs().getNumber('divisions-x');
		const divisionsY: number = this.getArgs().getNumber('divisions-y');

		await this.databaseService.init();
		
		this.thumbnails = new Thumbnails(
				this.databaseService,
				thumbnailsPath,
				divisionsX,
				divisionsY,
				this.getArgs().hasAttribute('once-only')
		);
		
		if (this.getArgs().hasAttribute('build')) {
			await this.thumbnails.build();
		} else {
			const src: string = this.getArgs().getString('src');
			const dest: string = this.getArgs().getString('dest');
			const quality: number = this.getArgs().getNumberOrUndefined('quality') || 92;

			const blocksX: number = this.getArgs().getNumber('blocks-x');
			
			let blocksY: number|undefined = this.getArgs().getNumberOrUndefined('blocks-y');
			const ratioY: number|undefined = this.getArgs().getNumberOrUndefined('ratio-y');

			commonsOutputDoing(`Loading source image from ${src}`);
			const image: Jimp = new Jimp();
			await image.loadFromFile(src);
			commonsOutputSuccess();

			const destWidth: number = this.getArgs().getNumberOrUndefined('dest-width') || image.getWidth();
			const ratio: number = image.getWidth() / image.getHeight();
			const destHeight: number = Math.round(destWidth / ratio);
			
			if (blocksY === undefined && ratioY !== undefined) {
				const blockW: number = image.getWidth() / blocksX;
				const blockH: number = blockW * ratioY;
				blocksY = Math.round(image.getHeight() / blockH);
				
				commonsOutputDebug(`Computed blocks-y for ratio ${ratioY} is ${blocksY}`);
			}
			
			if (blocksY === undefined) throw new Error('Either blocks-y or ratio-y needs to be specified');
			
			if ((image.getWidth() / blocksX) < divisionsX) throw new Error('Image width is not great enough to support this resolution of blocks and divisions');
			if ((image.getHeight() / blocksY) < divisionsY) throw new Error('Image height is not great enough to support this resolution of blocks and divisions');

			await this.thumbnails.loadToCache();
		
			const applied: Jimp = await this.apply(
					image,
					blocksX,
					blocksY,
					divisionsX,
					divisionsY,
					destWidth,
					destHeight
			);
			
			commonsOutputDoing(`Saving image to ${dest}`);
			await applied.save(dest, quality);
			commonsOutputSuccess();
		}
		
		process.exit(0);
	}
}
