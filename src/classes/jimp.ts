import { commonsAsyncInterval, commonsAsyncTimeout } from 'tscommons-es-async';
import { commonsAsyncAbortInterval } from 'tscommons-es-async';
import { ECommonsInvocation } from 'tscommons-es-async';
import { IRgba, IRgb } from 'tscommons-es-graphics';

import {
		commonsOutputDoing,
		commonsOutputSuccess,
		commonsOutputPercent
} from 'nodecommons-es-cli';
import { CommonsJimp } from 'nodecommons-es-graphics';

export class Jimp extends CommonsJimp {
	public clone(): Jimp {
		if (!this.image) throw new Error('Image not loaded');

		const clone: Jimp = new Jimp();
		clone.image = this.image.clone();
		
		return clone;
	}
	
	private async splitToDivisions(
			divisionsX: number,
			divisionsY: number
	): Promise<Jimp[][]> {
		// this isn't pixel perfect, but it's ok as it's only being used for rough comparison, not the output rendering
		const w: number = Math.floor(this.getWidth() / divisionsX);
		const h: number = Math.floor(this.getHeight() / divisionsY);
		
		const splits: Jimp[][] = [];
		
		for (let xi = divisionsX, x = 0; xi-- > 0; x += w) {
			const split: Jimp[] = [];
			for (let yi = divisionsY, y = 0; yi-- > 0; y += h) {
				const clone: Jimp = this.clone();
				await clone.crop(x, y, w, h);
				
				split.push(clone);
			}
			
			splits.push(split);
		}
		
		return splits;
	}

	private async toSingleRgb(): Promise<IRgb> {
		const pixels: IRgba[] = await this.toRgba();
		const single: IRgb = {
				// tslint:disable:object-literal-sort-keys
				red: 0,
				green: 0,
				blue: 0
				// tslint:enable:object-literal-sort-keys
		};
		
		const pixelsLength = pixels.length;
		for (let i = pixels.length; i-- > 0;) {
			single.red += pixels[i].red / pixelsLength;
			single.green += pixels[i].green / pixelsLength;
			single.blue += pixels[i].blue / pixelsLength;
		}
		
		return single;
	}
	
	public async summarise(
			divisionsX: number,
			divisionsY: number
	): Promise<IRgb[][]> {
		const splits: Jimp[][] = await this.splitToDivisions(divisionsX, divisionsY);
		
		const outs: IRgb[][] = [];
		for (const sub of splits) {
			const temp: IRgb[] = [];
			for (const split of sub) {
				const pixel: IRgb = await split.toSingleRgb();
				temp.push(pixel);
			}
			
			outs.push(temp);
		}
		
		return outs;
	}
	
	public async pixelise(
			blocksX: number,
			blocksY: number,
			divisionsX: number,
			divisionsY: number
	): Promise<IRgb[][][][]> {
		const w: number = this.getWidth() / blocksX;
		const h: number = this.getHeight() / blocksY;
		
		const rgbs: IRgb[][][][] = [];
		
		commonsOutputDoing(`Pixelising image`);

		const total: number = Math.round(this.getWidth() / w) * Math.round(this.getHeight() / h);
		let done: number = 0;
		
		commonsAsyncInterval(
				1000,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {
					commonsOutputPercent(done, total);
				},
				'apixelise-progress'
		);

		for (let x = 0; x < this.getWidth(); x += w) {
			const sub: IRgb[][][] = [];
			
			for (let y = 0; y < this.getHeight(); y += h) {
				done++;
				// for some reason this particular loop blocks the event loop, so we have to explicitly release things for the interval to be given chance to run
				// not sure why since there are several awaits in here?
				if (done % 10 === 0) await commonsAsyncTimeout(1);
				
				const tx: number = Math.floor(x);
				const ty: number = Math.floor(y);

				let tw: number = Math.ceil(w);
				let th: number = Math.ceil(h);
				while (x + tw > this.getWidth()) tw--;
				while (y + th > this.getHeight()) th--;
				if (tw <= 0 || th <= 0) continue;

				const clone: Jimp = this.clone();
				await clone.crop(tx, ty, w, h);
				
				sub.push(await clone.summarise(divisionsX, divisionsY));
			}
			
			rgbs.push(sub);
		}
		
		commonsAsyncAbortInterval('apixelise-progress');

		commonsOutputSuccess();
		
		return rgbs;
	}
}
