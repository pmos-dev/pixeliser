import { IRgb } from 'tscommons-es-graphics';

import { commonsFileListFilesWithExtensions } from 'nodecommons-es-file';
import {
		commonsOutputDoing,
		commonsOutputProgress,
		commonsOutputSuccess
} from 'nodecommons-es-cli';

import { Jimp } from '../classes/jimp';

import { DatabaseService } from '../services/database.service';

import { rgbMatrixToLinearNumberArray } from '../helpers/conversions';

import { IThumbnail } from '../interfaces/ithumbnail';

type TCache = IThumbnail & {
		linear: number[];
};

export class Thumbnails {
	private files: string[];
	private cache: TCache[]|undefined;
	
	constructor(
			private databaseService: DatabaseService,
			private path: string,
			private divisionsX: number,
			private divisionsY: number,
			private onceOnly: boolean = false
	) {
		this.files = commonsFileListFilesWithExtensions(path, [ 'jpg', 'png' ]);
	}
	
	public async build(): Promise<void> {
		commonsOutputDoing(`Building lookup for divisions ${this.divisionsX}:${this.divisionsY}`);

		// This is just so it isn't done full-size. It can be any low-ish number
		// multiples of 2 and 12 are good
		const comparisonWidth: number = 36;

		let tally: number = 0;
		for (const file of this.files) {
			commonsOutputProgress(++tally);
			
			try {
				const image: Jimp = new Jimp();
				await image.loadFromFile(`${this.path}/${file}`);
				
				const comparisonHeight: number = Math.round((image.getHeight() / image.getWidth()) * comparisonWidth);
				
				// to speed things up
				await image.resize(comparisonWidth, comparisonHeight);
				
				// the summary for the lookup is the whole image, not a block within it; divisions are not the same thing as blocks
				const summary: IRgb[][] = await image.summarise(
						this.divisionsX,
						this.divisionsY
				);
				
				await this.databaseService.populate({
						matrix: summary,
						file: file
				});
			} catch (e) {
				// ignore
			}
		}

		commonsOutputSuccess(this.files.length);
	}
	
	public async loadToCache(): Promise<void> {
		commonsOutputDoing(`Loading to cache`);
		this.cache = (await this.databaseService.listByDivisions(
				this.divisionsX,
				this.divisionsY
		))
				.map((thumbnail: IThumbnail): TCache => ({
						file: thumbnail.file,
						matrix: thumbnail.matrix,
						linear: rgbMatrixToLinearNumberArray(thumbnail.matrix)
				}));
		commonsOutputSuccess(this.cache.length);
	}
	
	public getClosest(block: IRgb[][]): string|undefined {
		// this is quite heavily optimised for speed rather than readability or minimal variable usage
		
		if (!this.cache) throw new Error('Cache is not loaded');

		const bvalues: number[] = rgbMatrixToLinearNumberArray(block);
		const bvaluesLength: number = bvalues.length;
		
		let best: TCache|undefined;
		let bestScore: number|undefined;
		let bestIndex: number|undefined;
		
		for (let i = this.cache!.length; i-- > 0;) {
			const entry: TCache = this.cache![i];
			
			let score: number = 0;
			for (let j = bvaluesLength; j-- > 0;) score += Math.abs(bvalues[j] - entry.linear[j]) / bvaluesLength;
			
			if (bestScore === undefined || score < bestScore) {
				best = entry;
				bestScore = score;
				bestIndex = i;
			}
		}
		
		if (!best) return undefined;
		
		if (this.onceOnly && bestIndex !== undefined) {
			this.cache!.splice(bestIndex, 1);
		}
		
		return best.file;
	}
	
	public async loadThumbnail(file: string): Promise<Jimp> {
		const thumbnail: Jimp = new Jimp();
		await thumbnail.loadFromFile(`${this.path}/${file}`);
		
		return thumbnail;
	}
}
