import { IRgb } from 'tscommons-es-graphics';

export interface IThumbnail {
		matrix: IRgb[][];
		file: string;
}
