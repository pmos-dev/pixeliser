import mongo from 'mongodb';

import { IRgb } from 'tscommons-es-graphics';

import { ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsMongodbService } from 'nodecommons-es-database-mongodb';

import {
		rgbMatrixToLinearNumberArray,
		linearNumberArrayToRgbMatrix
} from '../helpers/conversions';

import { IThumbnail } from '../interfaces/ithumbnail';

type TInternalThumbnail = {
		divisionsX: number;
		divisionsY: number;
		file: string;
		values: number[];
};

export class DatabaseService extends CommonsMongodbService {
	protected thumbnails: mongo.Collection<TInternalThumbnail>|undefined;

	constructor(credentials: ICommonsCredentials) {
		super(credentials);
	}
	
	public async init(): Promise<void> {
		await super.init();
		
		this.thumbnails = await this.ensureCollection('thumbnails');
		if (!this.thumbnails) throw new Error('Thumbnails collection cannot be instantiated.');

		await this.thumbnails.createIndex({ divisionsX: 1 }, { unique: false });
		await this.thumbnails.createIndex({ divisionsY: 1 }, { unique: false });
		await this.thumbnails.createIndex({ file: 1, divisionsX: 1, divisionsY: 1 }, { unique: true });
	}
	
	public async populate(thumbnail: IThumbnail): Promise<void> {
		if (!this.thumbnails) throw new Error('Thumbnails is not instantiated');
		
		const internal: TInternalThumbnail = {
				divisionsX: thumbnail.matrix.length,
				divisionsY: thumbnail.matrix[0].length,
				file: thumbnail.file,
				values: rgbMatrixToLinearNumberArray(thumbnail.matrix)
		};
		
		await this.thumbnails.insertOne(internal);
	}
	
	public async listByDivisions(
			divisionsX: number,
			divisionsY: number
	): Promise<IThumbnail[]> {
		if (!this.thumbnails) throw new Error('Thumbnails is not instantiated');

		const results: mongo.AggregationCursor<TInternalThumbnail> = this.thumbnails
				.aggregate<TInternalThumbnail>([
						{ $match: {
								divisionsX: divisionsX,
								divisionsY: divisionsY
						} },
						{ $project: {
								_id: false,
								file: true,
								values: true
						} }
					]);
		
		const array: IThumbnail[] = [];
		
		await this.forEachQueryResults(
				results,
				(_: unknown): _ is TInternalThumbnail => true,
				async (t: TInternalThumbnail): Promise<void> => {
					const matrix: IRgb[][] = linearNumberArrayToRgbMatrix(
							t.values,
							divisionsX,
							divisionsY
					);
					
					const thumbnail: IThumbnail = {
							matrix: matrix,
							file: t.file
					};
					
					array.push(thumbnail);
				}
		);
		
		return array;
	}
}
