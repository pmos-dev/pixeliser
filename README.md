# Welcome to thumbnail-pixeliser 👋
![Version](https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> Small app to render images using a database of other smaller sub-images.

### 🏠 [Homepage](https://bitbucket.org/pmos-dev/pixeliser/)

## Install

You need mongodb already installed. Name the database as thumbnailpixeliser and make sure it doesn't need credentials.

```sh
npm install thumbnail-pixeliser
```

## Usage

NB. node 15 is required to be installed. If you are using Node 14, then you will need to either invoke using the second syntaxes, or modify the thumbnail-pixeliser binary script to insert the following at the top:

```sh
#!/bin/sh 
":" //# comment; exec /usr/bin/env node --max-old-space-size=7192 --experimental-modules --experimental-specifier-resolution=node "$0" "$@"
```

Build the thumbnail database for specific divisions with:

```sh
thumbnail-pixeliser --build --thumbnails="<thumbnail source folder>" --divisions-x=<1,2,4,8,etc.> --divisions-y=<1,2,4,8,etc.>

node --max-old-space-size=7192 --experimental-modules --experimental-specifier-resolution=node dist/cli.js --build --thumbnails="<thumbnail source folder>" --divisions-x=<1,2,4,8,etc.> --divisions-y=<1,2,4,8,etc.>
```

Once you have built a database division set you can use it to render images, e.g. for an 8 division set...

```sh
thumbnail-pixeliser --src="<source image file>" --dest="<output image file.jpg>" --thumbnails="<thumbnail source folder>" --divisions-x=8 --divisions-y=8 --blocks-x=96 --ratio-y=1.333 --dest-width=3000 --once-only

node --max-old-space-size=7192 --experimental-modules --experimental-specifier-resolution=node dist/cli.js --src="<source image file>" --dest="<output image file.jpg>" --thumbnails="<thumbnail source folder>" --divisions-x=8 --divisions-y=8 --blocks-x=96 --ratio-y=1.333 --dest-width=3000 --once-only
```

src can be JPEG/PNG, dest is JPEG.

The thumbnails path needs to point to your large filestore of thumbnail images to use as pixels.

divisions-x/y are the number of sub-pixel divisions used in each thumbnail. For example, 4 means that each thumbnail is broken into 4 sub-pictures when used for matching against a desired pixel pattern. Used in combination with blocks.

blocks-x/y is the number of blocks to divide the source image into. For example, a 2000 width pixel source image broken into 100 blocks-x results in 20 pixel wide books. Instead of specifying the blocks-y, a ratio-y can be used instead, which automatically works out the blocks-y based upon blocks-x and that ratio. The ratio should correspond to the approximate thumbnail portrait/landscape ratio. 1.33 is good for book covers.

NB. The source image size must be equal to or greater than blocks * divisions. So width must be greater than blocks-x * divisions-x. To use small source images, either use a separate program to resize them to a larger size (quality loss through this isn't important), or reduce the number of divisions or blocks. For the above example, 8 divisions with 96 blocks means your source image needs to be a minimum of 768 pixels wide.

dest-width is the destination image width, which will probably want to be larger than the source width in order to have clarity of the thumbnails. Generally use something like 30 pixels * blocks, e.g. width = 30 * 100 blocks = 3000.

once-only tells the rendering engine to only use each thumbnail once. This requires a LOT more thumbnails, and can result in slightly more random colour matching, but is a more impressive picture. Omiting this allows each thumbnail to be used as many times as necessary to best match the pixel patterns. If you have a low number of thumbnails and a source image with relately large blocks of the same colour, this can result in the same thumbnail being repeated.

## Author

👤 **Pete Morris**


## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_